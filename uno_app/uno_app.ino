
#include <Arduino_FreeRTOS.h>
#include <Time.h>
//#include <HCSR04.h>
#include <queue.h>

#define TIME_MSG_LEN    11 // time sync to PC is HEADER followed by Unix time_t as ten ASCII digits
#define TIME_HEADER     'T' // Header tag for serial time sync message
#define TIME_REQUEST    7 // ASCII bell character requests a time sync message 

#define HCSR04_TRIGGER  2
#define HCSR04_ECHO     3

//HCSR04 hc(HCSR04_TRIGGER, HCSR04_ECHO);
        
QueueHandle_t queue;

// define two tasks for Blink & AnalogRead
void    TaskBlink       ( void *pvParameters );
void    TaskAnalogRead  ( void *pvParameters );
void    TaskSerialSend     ( void *pvParameters );

// the setup function runs once when you press reset or power the board
void setup() {
  queue = xQueueCreate( 10, sizeof( float ) );
  
  // Now set up two tasks to run independently.
  xTaskCreate(
    TaskBlink
    ,  (const portCHAR *)"Blink"   // A name just for humans
    ,  128  // Stack size
    ,  NULL
    ,  2  // priority
    ,  NULL );

  xTaskCreate(
    TaskAnalogRead
    ,  (const portCHAR *) "AnalogRead"
    ,  128 // This stack size can be checked & adjusted by reading Highwater
    ,  NULL
    ,  1  // priority
    ,  NULL );

      xTaskCreate(
    TaskSerialSend
    ,  (const portCHAR *) "AnalogRead"
    ,  128 // This stack size can be checked & adjusted by reading Highwater
    ,  NULL
    ,  1  // priority
    ,  NULL );

  // Now the task scheduler, which takes over control of scheduling individual tasks, is automatically started.
}

void loop()
{
  // Empty. Things are done in Tasks.
}

/*--------------------------------------------------*/
/*---------------------- Tasks ---------------------*/
/*--------------------------------------------------*/

void TaskBlink(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  // initialize digital pin 13 as an output.
  pinMode(13, OUTPUT);
  
  for (;;) // A Task shall never return or exit.
  {
    digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
    vTaskDelay( 1000 / portTICK_PERIOD_MS ); // wait for one second
    digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
    vTaskDelay( 1000 / portTICK_PERIOD_MS ); // wait for one second
  }
}

void TaskSerialSend(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  Serial.begin(9600);
  float element;
  for (;;) // A Task shall never return or exit.
  {
    xQueueReceive(queue, &element, portMAX_DELAY);
    Serial.println(element);
  }
}

void TaskAnalogRead(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  pinMode(HCSR04_TRIGGER, OUTPUT); // Sets the trigPin as an Output
  pinMode(HCSR04_ECHO, INPUT); // Sets the echoPin as an Input
  // initialize serial communication at 9600 bits per second:

  for (;;)
  {
    long duration;
    float distance;

    digitalWrite(HCSR04_TRIGGER, LOW);
    delayMicroseconds(2);
    // Sets the trigPin on HIGH state for 10 micro seconds
    digitalWrite(HCSR04_TRIGGER, HIGH);
    delayMicroseconds(10);
    digitalWrite(HCSR04_TRIGGER, LOW);
    // Reads the echoPin, returns the sound wave travel time in microseconds
    duration = pulseIn(HCSR04_ECHO, HIGH);
    // Calculating the distance
    distance= duration*0.034/2;
    
//    double distance = hc.dist();
    xQueueSend(queue, &distance, portMAX_DELAY);
    vTaskDelay(15);  // delay between reading distances
  }
}

