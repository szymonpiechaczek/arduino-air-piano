# Air piano on Arduino

Simple air piano implementation using Arduino board, HC-SR04, serial port and Python script.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

* Arduino board
* HC-SR04 sensor
* [Arduino IDE](https://www.arduino.cc/en/main/software/) - IDE for Arduino board
* [Python 3](https://www.python.org/download/releases/3.0/) with serial, re and pygame modules


### Installing and running

A step by step series of examples that tell you how to get a development env running

* Clone or Download repository

```
	git clone https://szymonpiechaczek@bitbucket.org/szymonpiechaczek/arduino-air-piano.git
```

or

```
	git clone git@bitbucket.org:szymonpiechaczek/arduino-air-piano.git
```

* Open arduino part in Arduino IDE, connect Arduino board to HC-SR04 (using PIN 2 as trigger, PIN 3 as echo, GND as GND and VCC as 5V) and to PC via serial port - check which serial port is used, compile and run.

* Next, open Python script and set serial port which is used, optionally changing this line:
```
	ser = serial.Serial('/dev/cu.usbmodem1411', 9600)
```

* Run Python script, turn on volume on PC and enjoy!

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
