import serial, pygame, re

DISTANCE = 3
BASE_DISTANCE = 4

ser = serial.Serial('/dev/cu.usbmodem1411', 9600)
# ser = serial.Serial('/dev/cu.usbmodem1421', 9600)
pygame.init()

instrument = "flute"
# instrument = "oboe"

c4_sound = pygame.mixer.Sound("../resources/sounds/" + instrument + "/c4.wav")
d4_sound = pygame.mixer.Sound("../resources/sounds/" + instrument + "/d4.wav")
e4_sound = pygame.mixer.Sound("../resources/sounds/" + instrument + "/e4.wav")
f4_sound = pygame.mixer.Sound("../resources/sounds/" + instrument + "/f4.wav")
g4_sound = pygame.mixer.Sound("../resources/sounds/" + instrument + "/g4.wav")
a4_sound = pygame.mixer.Sound("../resources/sounds/" + instrument + "/a4.wav")
b4_sound = pygame.mixer.Sound("../resources/sounds/" + instrument + "/b4.wav")
c5_sound = pygame.mixer.Sound("../resources/sounds/" + instrument + "/c5.wav")

while 1:
    serial_line = ser.readline()
    decoded = serial_line.decode("utf-8")

    searchObj = re.search(r'([0-9\.]*)', decoded, re.M|re.I)
    dist = int(float(searchObj.group()))

    print(dist)

    if dist < BASE_DISTANCE + DISTANCE:
        c4_sound.play()
    elif dist < BASE_DISTANCE + DISTANCE*2:
        d4_sound.play()
    elif dist < BASE_DISTANCE + DISTANCE*3:
        e4_sound.play()
    elif dist < BASE_DISTANCE + DISTANCE*4:
        f4_sound.play()
    elif dist < BASE_DISTANCE + DISTANCE*5:
        g4_sound.play()
    elif dist < BASE_DISTANCE + DISTANCE*6:
        a4_sound.play()
    elif dist < BASE_DISTANCE + DISTANCE*7:
        b4_sound.play()
    elif dist < BASE_DISTANCE + DISTANCE*10:
        c5_sound.play()